# Welcome to OmiLAXR

Welcome to the documentation page of the OmiLAXR Ecosystem! We are thrilled that you have taken the time to visit our platform and explore the possibilities of our software ecosystem.

This page is designed to be a comprehensive resource for educators, students, and developers who are interested in leveraging XR technology for educational purposes. Our goal is to provide a platform that is accessible, informative, and engaging for anyone who is passionate about the intersection of education and technology.

Whether you are just getting started with XR or are an experienced developer looking to push the boundaries of what is possible, this page has something for you. From technical guides all around the OmiLAXR Framework and best practices to case studies and inspiring examples of XR in action, our wiki is designed to be a valuable resource for anyone who is interested in using XR to enhance learning and improve educational outcomes.

We hope that you will find our ecosystem to be a useful and inspiring resource, and we look forward to engaging with you as you explore the exciting world of XR in education. Thank you for joining us on this journey!

# About OmiLAXR

Welcome to OmiLAXR, the Unity Framework specifically designed to support the development of educational extended reality applications. With OmiLAXR, you can create immersive and interactive learning experiences that engage students and enhance their understanding of complex concepts. Whether you're an educator, developer, or designer, OmiLAXR provides the tools and resources you need to create high-quality VR applications for the classroom or beyond. So join us and discover the endless possibilities of using VR technology to revolutionize the way we learn and teach.

The OmiLAXR ecosystem includes a collection of frameworks, tools in its workflow. Following structure is currently realized:

## Ecosystem
[OmiLAXR](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/omilaxr/): Is the base framework with combining useful functions, but also some core components for the OmiLAXR environment.

- [OmiLAXR.Adapters](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/adapters): Adapters for the OmiLAXR framework. Adapters are used to connect different tools and this framework.
    - Moodle: Coming soon
    - [SteamVR](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/adapters/omilaxr.adapters.steamvr): Adapter to make OmiLAXR compatible with SteamVR.
    - [ViveSR](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/adapters/omilaxr.adapters.vivesr):  Adapter to make OmiLAXR compatible with ViveSR.
    - [UnityXR](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/adapters/omilaxr.adapters.unityxr):  Adapter to make OmiLAXR compatible with UnityXR.
- OmiLAXR.Assistance (coming soon)
- [OmiLAXR.Analytics](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/omilaxr.analytics): Uses Unity editor to track developer activities inside of Unity and to track scripts which are using OmiLAXR itself.
- [OmiLAXR.WebXR](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/omilaxr.webxr): (in development) 
- [OmiLAXR.xAPI](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/omilaxr.xapi): A package containing useful tracking controllers and automized mechanism for xAPI tracking.

- [OmiLAXR Sandbox](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/omilaxr-sandbox): Development environment of OmiLAXR containing all related packages as git submodules.

## Tools for the OmiLAXR Ecosystem
- [xAPI Definitions Frontend](https://xapi.elearn.rwth-aachen.de/): Our [xAPI Definitions Registry](https://gitlab.com/learntech-rwth/xapi/) is defined by JSON files. For simplicity it can help to use the web frontend. For example, you can search there for existing defintions.
- [xAPI Definitions Fetcher](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/xapi-definitions-fetcher): Uses the [xAPI Registry](https://gitlab.com/learntech-rwth/xapi/) to transform their xAPI definitions into C# classes. This way you can get support from IDE while developing xAPI statements.
- [xAPI4Unity](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/xapi-4-unity): Uses the xAPI Definitions Fetcher to easily manage the xAPI definitions transformation inside of Unity.
- [ReCoPA (Researcher Companion Panel)](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/recopa): The Researcher Companion Panel can be used to control the xAPI tracking settings by web tool. It exists as web server and panel, but also as a standalone electron executable.
- [OpenTTS Unity](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/opentts-unity): Tool for using [OpenTTS](https://github.com/synesthesiam/opentts) simple inside of Unity. It uses [our deploy](https://opentts.elearn.rwth-aachen.de/) of the web service to generate the voices.

## Further useful tools for developer work
- [Learning Locker](https://lrs.elearn.rwth-aachen.de/): UI for managing Learning Record Stores.
- [OpenTTS](https://github.com/synesthesiam/opentts): Service for generating Text to Speech wav files.
