# OmiLAXR.Adapters.SteamVR

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Installation

Todo:
* Review and update this chapter incl. namings
* Reference Researcher Panel
* Write instructions for Researcher Panel

The installation requires several steps, depending on the already installed version. This includes the installation
of the dierent software as well as the integration of the module (VRDataCollector) into a new or an already
existing project.

### Software Installation
All steps are listed. If one of the software is already installed, this step can be skipped.

#### Steam and Steam VR
1. Create a Steam account
2. Download Steam client https://store.steampowered.com/about
3. Log into Steam client
4. Install SteamVR inside the Steam client (free of charge)
5. Start SteamVR and follow the setup steps