# OmiLAXR.Adapters.ViveSR



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!


## Vive SRanipal SDK
For the usage of the eye tracking system of the HTC Vive Pro Eye, the Vive SRanipal SDK needs to be installed.
Download VIVE SRanipalInstaller 1.1.2.0.msi on https://hub.vive.com/en-US/download.

---
If error [SRanipal] Initial Eye: INITIAL FAILED appears, restart computer and try it again.


#### 4.2.2 Unity
In addition to the website option, Unity has an additional setting option called UnitySetup. If this option is selected, the setting is made directly via Unity and the data collection starts as soon as the application has been started. If it is not selected, the system waits for settings from the website. An example of the setting via Unity


## Installation HTC Vive Pro Eye
Download and install the following softwares:
*  Steam
*  SteamVR
*  [SR_Runtime](https://dl.vive.com/SRSDK/runtime/VIVE_SRanipalInstaller.msi)


Follow the following instructions:
1.  Start SR_Runtime
2.  Start SteamVR
3.  Put on HMD
4.  open Steam Dashboard
5.  Open point Vive Pro Eye
6.  Calibrate HMD