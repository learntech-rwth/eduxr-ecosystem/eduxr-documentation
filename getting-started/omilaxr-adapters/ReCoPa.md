# OmiLAXR.Adapters.ReCoPa



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

#### 3.1.4 Node.js and npm
If not already installed, follow the instructions on https://www.npmjs.com/get-npm


#### 3.1.5 VRDataCollector
*  Download VRDataCollector from as zip file
*  Unzip this file
*  Place the unzipped folder at the desired location in the system

## Configuration

**Backend**

| Via start file:                      | Via terminal/command line:               |
|--------------------------------------|------------------------------------------|
| Open file explorer                   | Open a terminal/command line             |
| Navigate into folder VRDataCollector | Navigate into the folder WebsocketServer |
| Double click on installBackend.bat     | Type npm install                           |



**Frontend**

| Via start file:                      | Via terminal/command line:               |
|--------------------------------------|------------------------------------------|
| Open file explorer                   | Open a terminal/command line             |
| Navigate into folder VRDataCollector | Navigate into the folder Frontend |
| Double click on installFrontend.bat     | Type npm install                           |

### 4.2 User Setup
Before collecting learning data in virtual environments some settings needs to be done. Therefore, two different options exists:
1. Unity
2. Website
   Independent of the choice of setup, the following settings can/must be filled out:


#### 4.2.1 Website
The settings via the website is similar to the one in Unity except that the application will not start directly
without getting the setting options from the website. Start first backend and afterwards frontend before starting
Unity application:



**Backend**

(coming soon)

**Frontend**

| Via start file:                      | Via terminal/command line:               |
|--------------------------------------|------------------------------------------|
| Open file explorer                   | Open a terminal/command line             |
| Navigate into folder VRDataCollector | Navigate into the folder Frontend |
| Double click on startFrontend.bat     | Type npm start Application                          |



After the start of the three steps in this order, the application sends all necessary information via the backend to the frontend. All setup options can now be done via the website by typing into the corresponding text field or choosing all necessary options in the dropdown boxes. When all settings have been made, the start button must be pressed to send all information back to the application.
Additionally to the setup options, setup templates can be both loaded and saved via the box Select Setup Template. Whe choosing a template from the dropdown menu, the setup will be loaded. To save a new template type a template name into the box Name Template and click on save. The name of the template needs to be unique. The user receives feedback about failed/successful storage.
