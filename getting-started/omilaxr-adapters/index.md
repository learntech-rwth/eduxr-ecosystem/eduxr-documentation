﻿# EduXR.Adapters

The idea of EduXR.Adapters is to make EduXR compatible with each Unity framework by reducing the Framework itself to an
abstract level and use instead some further packages to specialize the use case.

This way the automized xAPI generation can react on each interaction component events (laser pointer, hand interaction, teleport etc.).

For example, the EduXR Framework SteamVR is to connect EduXR to an application which is based on
SteamVR. 

But also connections to more external tools are possible. One example is the adapter for the
Research Companion Panel ReCoPa. This adapter is holding components which are communicating between this framework and the
external application. 

Look into each adapter in separate to get more details about how to use it and what these adapters can do.

## How to implement a new adapter

If you need a adapter, mostly you don't have and concrete rules.

But if you are using a XR interaction framework which is not supported yet (e.g. VRTK) you have to implement an own EduXR adapter.
Further, if your XR application is not using the typical way of a framework (e.g. you are using UnityXR in a very special way), you also need an own adapter.

For this it is very important to create component implementing the following interfaces:
- [IPlayer](https://learntech-rwth.gitlab.io/eduxr-ecosystem/eduxr-documentation/scripts/EduXR.IPlayer.html): As on each XR framework the player is represented by different classes, this interface is designed abstract and is used by EduXR for player interactions. Thus, your player adapter should implement this interface and connect it with the player object of your project.
- [XR_Framework_Adapter](https://learntech-rwth.gitlab.io/eduxr-ecosystem/eduxr-documentation/scripts/EduXR.Adapters.XR_Framework_Adapter.html): A component implementing this interface is the main entry point for your adapter. This component must import everything your adapter needs.
- [ITeleportAdapter](https://learntech-rwth.gitlab.io/eduxr-ecosystem/eduxr-documentation/scripts/EduXR.xAPI.Tracking.Teleport.ITeleportAdapter.html): This is needed to trigger teleport events

### Dependencies
Mostly a adapter needs to include EduXR and EduXR.xAPI to support xAPI automation. But is it also typical to include more further packages for your adapter.