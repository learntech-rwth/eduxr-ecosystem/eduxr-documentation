# Getting started

To install the OmiLAXR Framework you have to import the Unity packages.

## Using the example project & scenes
Each OmiLAXR package has the [folder structure](https://docs.unity3d.com/Manual/cus-layout.html) of Unity.
In other words, you can find components, prefabs and sample scenes for each OmiLAXR module.

So you can learn how to use a package, by looking into the Samples scenes.

## Importing OmiLAXR packages
Even if there are many ways to import packages (see [here](https://docs.unity3d.com/2021.1/Documentation/Manual/upm-ui-actions.html)).

We recommend to import Unity packages via name or git. This way you can update the packages easier.

Click inside the Unity editor on `Window > Package Manager > + > Add package by git URL` (or) `Window > Package Manager > + > Add package by name`.

As OmiLAXR is not published in Unity repositories, it can be only imported via git. Use for this the Git URL of a OmiLAXR package (e.g. https://gitlab.com/learntech-rwth/omilaxr-ecosystem/omilaxr.git).

## Needed Third Party packages
To use OmiLAXR, please import following third party packages.
We restructured some packages in a way, that they are following the Unity package rules.
For orientation we listed here the repositories to the packages.

Each of our packages will list a dependencies list of packages with a URL or a name we recommend to import.

## Enable OmiLAXR.Analytics
In order to proceed with this study, it is necessary for you to activate OmiLAXR Analytics. You can follow the instructions provided on [this page](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/OmiLAXR.Analytics.html) to install it. This particular package can be installed without the need to install any other OmiLAXR packages, as it is independent. Once you have read and agreed to the terms and conditions, you can proceed with the installation of OmiLAXR. If you are working on a computer with multiple users, be sure to input all names used in the pretest. It is recommended that you use the same name as you did in the pretest.

## xAPI Registry for Unity
You can use the [xAPI Registry](https://xapi.elearn.rwth-aachen.de/) of LuFGi9 RWTH Aachen University inside of Unity and C#. To achieve this, it is needed to install the [xAPI4Unity](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/xAPI4Unity.html) package. Follow the instructions of this link to install this package and do use xAPI in your project. 

## Enable OmiLAXR Analytics for xAPI4Unity
One needed addon for OmiLAXR.Analytics is [OmiLAXR.Analytics.xAPI4Unity](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/analytics/omilaxr.analytics.xapi4unity). This one is needed to enable tracking of the xAPI4Unity editor UI.

## Starting with OmiLAXR
OmiLAXR consists of a base package, which is required for each further OmiLAXR package, and additional packages are installed on top of it. 

### Install OmiLAXR base package

The initial package you must install is the [OmiLAXR](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/OmiLAXR.html) package. 

### Install OmiLAXR.xAPI

In order to use the xAPI tracking system, you need to install [OmiLAXR.xAPI](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/OmiLAXR.xAPI.html). This package provides you components and tracking systems for different use-cases. This tracking system is using the [xAPI Registry](https://xapi.elearn.rwth-aachen.de/) and depends on a registry collection created by [xAPI4Unity](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/xAPI4Unity.html).

### Install needed OmiLAXR Adapters

Next, if you need to check if you need some [OmiLAXR Adapters](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/omilaxr-adapters/index.html).
Please check out following adapters. Attention: you may not need every adapter.

- [ReCoPa Adapter](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/omilaxr-adapters/ReCoPa.html): Let your XR application communicate with the [Researcher Companion Panel](https://gitlab.com/learntech-rwth/omilaxr-ecosystem/recopa/recopa-electron). 
- [SteamVR Adapter](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/omilaxr-adapters/SteamVR.html): If your project is based on the [SteamVR Unity plug-in](https://valvesoftware.github.io/steamvr_unity_plugin/) you need to install this adapter.
- [UnityXR Adapter](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/omilaxr-adapters/UnityXR.html): If your project is based on the [Unity XR plug-in](https://docs.unity3d.com/Manual/XR.html) you need to install this adapter.
- [ViveSR Adapter](https://learntech-rwth.gitlab.io/omilaxr-ecosystem/omilaxr-documentation/getting-started/omilaxr-adapters/ViveSR.html): If your project needs ViveSR support, you need to install this adapter.

